
pub fn reversing(input: &str) -> String {
    unimplemented!("Write a function to reverse {input}");
}

pub fn reverse(input: &str) -> String {
    let s = String::from(input);
    let reversed = s.chars().rev().collect::<String>();
    return reversed;
}

pub fn second_reverse(input: &str) -> String {
    let s: String = String::from(input);
    let reversed: String = s.chars().rev().collect();
    return reversed;
}